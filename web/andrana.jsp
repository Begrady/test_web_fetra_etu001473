<%-- 
    Document   : test
    Created on : 14 nov. 2022, 11:13:46
    Author     : Ambinintsoa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%
            if(request.getAttribute("testdata") != null) {
                String[] res = (String[])request.getAttribute("testdata");
                out.print(res[0]+", "+res[1]);
            } else out.print("no data, ok");
        %>
    </body>
</html>
